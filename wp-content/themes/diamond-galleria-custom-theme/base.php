<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>
<!doctype html>

<html <?php language_attributes();?>>
    <?php get_template_part('templates/head');
    if (!is_page_template('page-splash.php')):
    ?>
    <body <?php body_class();?>>
        <?php get_template_part('tag-manager');?>
        <!--[if IE]>
            <div class="alert alert-warning">
            <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage');?>
            </div>
        <![endif]-->

        <?php do_action('get_header');

        if (is_singular('product')) {
            get_template_part('templates/products-modal');
        }

        if (is_archive('events') || is_singular('events')) {
            get_template_part('templates/rsvp-modal');
        }

        get_template_part('templates/header');

        ?>

        <div class="site-wrapper" role="document">

            <?php if (is_page_template('page-home.php')): ?>
                <?php get_template_part('templates/slider');?>
            <?php endif;?>

            <?php // Breadcrumbs
            if (is_home()) {
                yoast_breadcrumb('<nav id="breadcrumbs"><div class="inner">', '</div></nav>');
            }
            ?>

            <?php if (!is_page_template('page-home.php') && !is_archive()): ?>
                <?php get_template_part('templates/page', 'header');?>
            <?php endif;?>

            <div class="flexible <?php if(is_front_page()) { echo 'flexible--full'; } ?>">
                <main class="main <?php if(is_front_page()) { echo 'main--full'; } ?>">
                  <?php include Wrapper\template_path();?>
                </main><!-- /.main -->
                <?php if (Setup\display_sidebar()): ?>
                    <aside class="site-sidebar">
                        <?php include Wrapper\sidebar_path();?>
                    </aside><!-- /.site-sidebar -->
                <?php endif;?>
            </div>

        </div><!-- /.site-wrapper -->

        <?php
        do_action('get_footer');
        get_template_part('templates/footer');
        wp_footer();
        ?>

        <?php else: ?>

        <?php
            include Wrapper\template_path();
            do_action('get_footer');
            wp_footer();
        ?>
        <?php endif;?>
    </body>
</html>
