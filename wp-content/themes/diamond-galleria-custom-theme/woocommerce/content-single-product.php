<?php
use Roots\Sage\Extras;

/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
  do_action( 'woocommerce_before_single_product' );

  if ( post_password_required() ) {
     echo get_the_password_form();
     return;
 }
 ?>

 <div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

   <?php
		/**
		 * woocommerce_before_single_product_summary hook.
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
       ?>

       <div class="summary entry-summary">

          <?php
			/**
			 * woocommerce_single_product_summary hook.
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 */
			do_action( 'woocommerce_single_product_summary' );
          ?>

      </div><!-- .summary -->



      <div class="single-product-sidebar">
        <div class="single-product-widget">
            <a data-modal="book-appointment" class="prodModal" href="#book-appointment"><?php Extras\svg('time'); ?><h3>Book an Appointment</h3></a>
        </div>
         <div class="single-product-widget">
            <!-- data-modal="finance-options"
            class="prodModal"
            href="#finance-options" -->
            <a href="/financing/"><?php Extras\svg('business'); ?><h3>Finance Options</h3></a>
        </div>
         <div class="single-product-widget">
            <a data-modal="share-product" class="prodModal" href="#share-product"><?php Extras\svg('share'); ?><h3>Share this Item</h3></a>
        </div>
         <div class="single-product-widget">
            <a data-modal="email-a-friend" class="prodModal" href="#email-a-friend"><?php Extras\svg('letter'); ?><h3>Email to a Friend</h3></a>
        </div>
    </div>
    <?php
		/**
		 * woocommerce_after_single_product_summary hook.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
       ?>

       <meta itemprop="url" content="<?php the_permalink(); ?>" />

   </div><!-- #product-<?php the_ID(); ?> -->

   <?php do_action( 'woocommerce_after_single_product' ); ?>
