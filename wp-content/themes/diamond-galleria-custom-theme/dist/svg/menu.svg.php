<svg version="1.1" id="Untitled-2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20 18" xml:space="preserve">
<g id="Layer_x25_201">
    <rect class="svg-burger-top" width="100%" rx="2" ry="2" height="3"/>
    <rect class="svg-burger-middle" y="7" width="100%" rx="2" ry="2" height="3"/>
    <rect class="svg-burger-bottom" y="14" width="100%" rx="2" ry="2" height="3"/>
</g>
</svg>
