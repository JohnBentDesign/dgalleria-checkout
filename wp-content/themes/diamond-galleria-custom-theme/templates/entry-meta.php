<div class="meta">
	<div>
		<time datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
		<p class="categories"><?php the_category( ', ' ); ?></p>
	</div>
	<?php get_template_part('templates/featured-share-icons'); ?>
</div>
