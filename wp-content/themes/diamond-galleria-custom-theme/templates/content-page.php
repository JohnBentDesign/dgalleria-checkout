<?php
// If a single designer page...
if (is_singular('designers')) :
    get_template_part('templates/content-designers-single');
    get_template_part('templates/single-designer-products');
endif;

if (have_rows('section')) : ?>
<div class="sectional <?php if(is_front_page()) { echo 'sectional--full'; } ?>">

    <?php
      $sections = get_field('section');

      if ($sections) :
        $i = 1;
        $rowing = 0;
        foreach ($sections as $section) :
          // Our columns need to be cleared at the right times... So let's row!
          $sectionWidth   = $section['section_width'];
          $rowContainer   = section_rowing($sectionWidth, $rowing, $i);
          $rowing         = $rowContainer['rowing'];

          echo $rowContainer['open'];

          // Build our section
          $build = new section_build($section);

          // Output our column
          $build->section_construct($i);

          echo $rowContainer['close'];

          // Increment our section
          $i++;
        endforeach;
      endif;
    ?>
</div>
<?php endif;
