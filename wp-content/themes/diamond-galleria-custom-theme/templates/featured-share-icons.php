<?php

$socialIcons = get_social_networks();
$social_string = "";

$count = 0;
foreach ($socialIcons as $key => $icon) {
        if ($count <= 7) {

            // Fix for google plus
            if ($key === 'google-plus') {
                $iconName = "Google+";
                $icon_text = '<span class="social__text">' . $iconName . '</span>';
            }
            else {
                $iconName = $key;
                $icon_text = '<span class="social__text">' . $iconName . '</span>';
            }

            $svg = file_get_contents(get_stylesheet_directory_uri() . '/dist/svg/' . $key . '.svg.php');
            $social_string .=
            '<li class="social"><a target="_blank" href="'. $icon .'">' . $icon_text .
            '<span class="social__icon">' . $svg . '</span></a></li>';
            $count++;
        }
}

echo '<div class="share"><ul class="social-icons">' . $social_string . '</ul></div>';

