<div id="prodModal" class="modal-wrapper">
    <div class="modal">
        <span class="modal-dismiss">+</span>
        <h3 id="modalTitle"></h3>
        <div class="modal-inner" data-modal="book-appointment">
            <?php gravity_form('Request an Appointment', false, true, false, '', false); ?>
        </div>
        <!-- <div class="modal-inner" data-modal="finance-options"></div> -->
        <div class="modal-inner" data-modal="share-product">
            <share-button id="share-button-product">Share</share-button>
        </div>
        <div class="modal-inner" data-modal="email-a-friend">
            <?php gravity_form('Client Friend Emailer', false, true, false, '', false); ?>
        </div>
    </div>
</div>