<?php use Roots\Sage\Extras; ?>

<footer class="site-footer">

	<div class="footer-widgets">

    <div class="footer__inner footer__inner--quarter">
      <?php
                // Get the title of the menu
      $menu_location = 'footer_navigation_1';
      $menu_locations = get_nav_menu_locations();
      $menu_object = (isset($menu_locations[$menu_location]) ? wp_get_nav_menu_object($menu_locations[$menu_location]) : null);
      $menu_name = (isset($menu_object->name) ? $menu_object->name : '');

                // echo '<h4>' . esc_html($menu_name) . '</h4>';

                // Now display the menu!
      if (has_nav_menu('footer_navigation_1')) :
        wp_nav_menu(['theme_location' => 'footer_navigation_1', 'menu_class' => 'footer-nav', 'menu_id' => 'footer-nav-1']);
      endif;
      ?>
    </div>

    <div class="footer__inner footer__inner--quarter">
      <?php
                // Get the title of the menu
      $menu_location = 'footer_navigation_2';
      $menu_locations = get_nav_menu_locations();
      $menu_object = (isset($menu_locations[$menu_location]) ? wp_get_nav_menu_object($menu_locations[$menu_location]) : null);
      $menu_name = (isset($menu_object->name) ? $menu_object->name : '');

      echo '<h4>' . esc_html($menu_name) . '</h4>';

                // Now display the menu!
      if (has_nav_menu('footer_navigation_2')) :
        wp_nav_menu(['theme_location' => 'footer_navigation_2', 'menu_class' => 'footer-nav', 'menu_id' => 'footer-nav-2']);
      endif;
      ?>
    </div>

    <div class="footer__inner footer__inner--sixth">
      <h4>Our Store:</h4>
      <p>
        6245 Vogel Rd.<br>
        Evansville, IN 47715<br>
        <a href="https://www.google.com/maps/place/The+Diamond+Galleria/@37.987219,-87.47452,15z/data=!4m2!3m1!1s0x0:0x5ac7b643550de2ce">Get Directions</a>
      </p>
    </div>

    <div class="footer__inner footer__inner--sixth">
      <h4>Our Hours:</h4>
      <ol>
        <li><strong>Monday - Thursday: </strong>10AM–6PM</li>
        <li><strong>Friday: </strong>10AM–7PM</li>
        <li><strong>Saturday: </strong> 10AM–5PM</li>
        <li><strong>Sunday: </strong>  Closed</li>
      </ol>
    </div>

    <div class="footer__inner footer__inner--sixth">
      <h4>Stay In Touch:</h4>
      <?php Extras\social_icons(); ?>
    </div>

    <div class="footer__inner footer__inner--two-thirds">
      <h4>Certifications</h4>
      <ul class="certifications">
        <li>     <img class="certification" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/dg-gia.png" alt="Gia Certified"></li>
        <li>        <img class="certification"  src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/dg-igl.png" alt="IGL Certified"></li>
        <li>        <img class="certification" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/dg-egl.png" alt="EGL Certified"></li>
        <li><a href="http://www.preferredjewelersinternational.com/"><img class="certification" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/dg-pj.png" alt="Preferred Jeweler Certified"></a></li>
      </ul>
    </div>

    <div class="footer__inner footer__inner--third">
      <h4>Join Our Mailing List</h4>
      <?= do_shortcode('[gravityform id="4" title="false" description="false" tabindex=32]'); ?>
    </div>

  </div>

  <div class="site-footer__banner">
    <span><small>&copy;<?php echo date("Y"); ?> <?php bloginfo('name'); ?> All Rights Reserved.</small></span>
  </div>

</div>

</footer>
