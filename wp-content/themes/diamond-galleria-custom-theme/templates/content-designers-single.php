<?php use Roots\Sage\Titles; ?>

<div class="designer">

	<div class="designer-featured">
    	<?php
        $featured       = get_post_thumbnail_id();
        $featuredAlt    = get_post_meta($featured, '_wp_attachment_image_alt', true);
        $featuredSrc    = wp_get_attachment_image_url($featured, 'medium');
        $featuredSrcSet = wp_get_attachment_image_srcset($featured, 'medium');
        ?>

        <img src="<?php echo esc_url($featuredSrc); ?>" srcset="<?php echo esc_attr($featuredSrcSet ); ?>" sizes="(max-width: 50em) 87vw, 680px" alt="<?php echo $featuredAlt; ?>">
    </div>

    <div class="designer-content">
        <div class="designer-imagery">
            <div class="designer-logo">
                <?php
                $designer = get_field('designer_logo_single');
                $designerAlt = get_post_meta($designer, '_wp_attachment_image_alt', true);
                $designerSrc = wp_get_attachment_image_url($designer, 'medium');
                $designerSrcSet = wp_get_attachment_image_srcset($designer, 'medium');
                ?>
                <img src="<?php echo esc_url($designerSrc); ?>" srcset="<?php echo esc_attr($designerSrcSet); ?>" sizes="(max-width: 50em) 87vw, 680px" alt="<?php echo $designerAlt; ?>">
                <h1><?= Titles\title(); ?></h1>
            </div> 

            <?php if(have_rows('designer_medals')) :
                echo '<div class="designer-cert">';

                while (have_rows('designer_medals')) :
                    the_row();
                    $cert 		= get_sub_field('certification_medal');
                    $certAlt 	= get_post_meta($cert, '_wp_attachment_image_alt', true);
                    $certSrc 	= wp_get_attachment_image_url($cert, 'medium');
                    $certSrcSet = wp_get_attachment_image_srcset($cert, 'medium');
            ?>
                   <div class="certs"><img src="<?php echo esc_url( $certSrc ); ?>" srcset="<?php echo esc_attr( $certSrcSet ); ?>" sizes="(max-width: 50em) 87vw, 680px" alt="<?php echo $certAlt; ?>"></div>
            <?php
                endwhile;
                echo '</div>';
                endif;
            ?>
        </div>

        <?php the_field('designer_overview'); ?>

    </div>

</div>
