<?php // Slider
use Roots\Sage\Extras as Extras;

if (have_rows('home_slider')) :

	$transition      = get_field('home_slide_animate');
	$timer           = get_field('home_slide_time');
	$nodes           = get_field('home_slide_nodes');
	$arrows          = get_field('home_slide_arrows');
	$animation_speed = get_field('home_slide_time_animate');

	// Slick lets you drop in the settings as a data attribute now, so we can
	// still do our lovely footer js and get our acf settings :)
	$slick_config = array(
        "fade"          => filter_var($transition, FILTER_VALIDATE_BOOLEAN),
        "dots"          => $nodes,
        "arrows"        => $arrows,
        "autoplay"      => true,
        "autoplaySpeed" => $timer
	);
?>
	<div id="slider" class="slider" data-slick='<?php echo json_encode($slick_config); ?>'>
		<?php
		  	while (have_rows('home_slider')) : the_row();
				$slide_title 	= get_sub_field('title');
				$slide_copy  	= get_sub_field('content');
				$slide_color 	= 'slide--' . get_sub_field('image_text');
				$slide_side 	= get_sub_field('content_side') ? ' slide--' . get_sub_field('content_side') : '';

				$cta_text    = get_sub_field('cta_text');
				if (get_sub_field('cta_type_display') === 'button') {
					$cta_button  = 'btn btn--alt';
				} else {
					$cta_button = '';
				}

				if (get_sub_field('cta_type') == 'internal') {
					$cta_link = get_sub_field('cta_internal');
				} elseif (get_sub_field('cta_type') == 'external') {
					$cta_link = get_sub_field('cta_external');
				}
		?>
				<div class="slide <?= $slide_color ?>" style="background-image: url('<?php echo get_sub_field('image')['url']; ?>')">
					<div class="slide-content<?= $slide_side; ?>">
						<?php if ($slide_title) : ?>
							<h2 class="slide-title"><?= $slide_title ?></h2>
							<hr>
						<?php endif; ?>
						<?php if ($slide_copy) : ?>
							<div class="slide-copy">
								<?= $slide_copy ?>
							</div>
						<?php endif; ?>
						<?php if ($cta_text) : ?>
							<a class="<?= $cta_button ?>" href="<?= $cta_link ?>" <?= $cta_blank ?>>
								<?= $cta_text ?>
							</a>
						<?php endif; ?>
					</div>
					<div class="slide--overlay"></div>
				</div>
		<?php endwhile; ?>
	</div>
<?php endif; ?>
