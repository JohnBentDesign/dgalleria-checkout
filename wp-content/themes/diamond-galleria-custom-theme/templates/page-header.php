<?php use Roots\Sage\Extras;
use Roots\Sage\Titles;
?>

<div class="page-header">
    <?php if (!is_home() && !is_category() && !is_archive() && is_page()): ?>
        <?=Extras\ttg_featured_image();?>
    <?php endif;?>

    <?php
    // Breadcrumbs
    if (!is_home() && function_exists('yoast_breadcrumb') && !is_page_template('page-home.php') && !is_archive()) {
        yoast_breadcrumb('<nav id="breadcrumbs"><div class="inner">', '</div></nav>');
    }
    ?>

    <?php
    // Center the title if the first section is set to center
    $centerText = '';
    if (have_rows('section')) :
        $i = 0;
        while (have_rows('section')) :
            the_row();
            if ($i == 0) {
                $centerText = get_sub_field('content_center') ? ' class="text-center"' : '';
                $i++;
            }
        endwhile;
    endif;
    ?>
    <?php if (
        (get_field('page_title') != true)
        && !is_home()
        && (get_post_type() != 'events')
        && !is_single()
        ): ?>
        <h1<?=$centerText;?>><?=Titles\title(); ?></h1>
    <?php endif;?>
</div>
