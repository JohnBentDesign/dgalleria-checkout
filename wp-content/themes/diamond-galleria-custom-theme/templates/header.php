<?php
use Roots\Sage\Dropline;
use Roots\Sage\Extras;
?>
<nav class="touch-controls">
  <ul class="touch-controls__list">
    <li><a class="touch-link" href="tel://812-477-1388">
      <span class="screen-reader-text">Call</span>
      <?php Extras\svg('phone');?>
    </a></li>
    <li><a class="touch-link" target="_blank" href="https://www.google.com/maps/dir//The+Diamond+Galleria,+6245+Vogel+Rd,+Evansville,+IN+47715/@37.9872232,-87.476714,17z/data=!4m13!1m4!3m3!1s0x886e2b7142852b4f:0x5ac7b643550de2ce!2sThe+Diamond+Galleria!3b1!4m7!1m0!1m5!1m1!1s0x886e2b7142852b4f:0x5ac7b643550de2ce!2m2!1d-87.47452!2d37.987219">
      <span class="screen-reader-text">Get Directions</span>
      <?php Extras\svg('location');?>
    </a></li>
        <?php /*
<li><a id="touch-link-search" class="touch-link" href="#touch-search">
<span class="screen-reader-text">Search</span>
<?php Extras\svg('search'); ?>
<div id="touch-search" class="touch-search">
<?php get_search_form(); ?>
</div>
</a></li>
 */?>
 <li><a id="mobile-nav-trigger" class="touch-link" href="#mobile-nav">
  <span class="screen-reader-text">Menu</span>
  <?php Extras\svg('menu');?>
</a></li>
</ul>
</nav>
<div class="mobile-nav-wrap">
  <?php
  if (has_nav_menu('primary_navigation')) :

    $walker = new Dropline\droplineWalker;

  wp_nav_menu([
    'theme_location' => 'primary_navigation',
    'menu_class' => 'mobile-nav',
    'menu_id' => 'mobile-nav',
    'walker' => $walker,
    ]);
  endif;
  ?>
</div>
<header class="site-header">
  <div class="masthead">
    <div class="masthead-inner">
      <div class="masthead__child">
        <ul class="brand-meta">
          <li><a href="tel://812-477-1388" class="header-phone">T: (812) 477-1388 </a></li>
          <li><a href="https://www.google.com/maps/place/The+Diamond+Galleria/@37.987219,-87.47452,15z/data=!4m2!3m1!1s0x0:0x5ac7b643550de2ce">6245 Vogel Rd, Evansville, IN 47715</a></li>
        </ul>
      </div>
      <div class="masthead__child">
        <a class="brand" href="<?=esc_url(home_url('/'));?>">
          <?php // bloginfo('name'); ?>
          <?php
          $logoID = get_theme_mod('ttg_add_logo');
          $logoURL = wp_get_attachment_image_src($logoID, 'medium');
          ?>
          <img src="<?=$logoURL[0]?>" alt="<?=bloginfo('name')?>">
        </a>
      </div>
      <div class="masthead__child cart-account">

      <div class="account">
      <a href="<?php echo site_url().'/my-account/'; ?>">
         <?php Extras\svg('shoppingbag'); ?><span>Account</span>
      </a>
      </div>
     
            <?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
 
        $count = WC()->cart->cart_contents_count;
         
        ?><div class="cart-contents">
        <a href="<?php echo WC()->cart->get_cart_url(); ?>"><?php Extras\svg('shoppingcart'); ?><span>View Cart</span></a>
          <?php if($count) { ?>
            <a class="total" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php if ( $count > 0 ) echo   $count ; ?></a>
            
            <?php  } else {
          
                   }

                } ?>
          </div>
        

        <?php // get_search_form(); ?>
      </div>
    </div>
  </div>
</div>
<nav class="nav-primary">
  <div id="nav-trigger" class="nav-trigger">Menu<div class="hamburger"></div></div>
  <?php
  if (has_nav_menu('primary_navigation')) {
    $walker = new Dropline\droplineWalker;

    wp_nav_menu([
      'theme_location' => 'primary_navigation',
      'menu_class' => 'nav',
      'menu_id' => 'nav',
      'walker' => $walker,
      ]);
  }
  ?>
</nav>
</header>

<?php
// Callout
$calloutDisplay = get_field('global_cta', 'options');
$calloutHome = get_field('global_cta_home', 'options');
$title = get_field('global_cta_title', 'options');
$content = get_field('global_cta_content', 'options');
$linkURL = get_field('global_cta_link', 'options');
$bgImg = get_field('global_cta_bg_image', 'options');
$bgColor = get_field('global_cta_bg_color', 'options');

if (($calloutDisplay && (is_front_page() && $calloutHome)) || ($calloutDisplay && !is_front_page())):
  ?>
<div class="global-callout text-center <?=$bgColor;?>"<?php if ($bgImg) {echo ' style="background-image: url(' . $bgImg['url'] . ')"';}?>>
  <?php
  if ($linkURL) {
    echo '<a href="' . $linkURL . '" class="cta-link">';
  }
  if ($title || $content) {
    echo '<div class="content">';
  }
  if ($title) {
    echo '<h2>' . $title . '</h2>';
  }
  if ($content) {
    echo $content;
  }
  if ($title || $content) {
    echo '</div>';
  }
  if ($bgImg) {
    echo '<div class="overlay overlay--' . $bgColor . '"></div>';
  }
  if ($linkURL) {
    echo '</a>';
  }
  ?>
</div>
<?php endif;
