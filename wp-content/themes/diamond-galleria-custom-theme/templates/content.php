<article <?php post_class('excerpt'); ?>>
	<?php if(has_post_thumbnail()): ?>
		<div class="featured">
			<?php the_post_thumbnail('large'); ?>
		</div>
	<?php endif; ?>
	<div class="content">
		<header>
			<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<?php get_template_part('templates/entry-meta'); ?>
		</header>
		<!-- <div class="entry-summary">
			<?php the_excerpt(); ?>
		</div> -->
        <a class="btn" href="<?php the_permalink(); ?>">Read More</a>
	</div>
</article>
