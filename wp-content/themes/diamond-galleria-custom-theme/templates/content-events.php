<?php
use Roots\Sage\Extras;

// Date Config
$today      = new DateTime('NOW');
$today->modify('-1 day');
$start_date = DateTime::createFromFormat('Ymd', get_field('date'));
$end_date   = DateTime::createFromFormat('Ymd', get_field('end_date'));

// Set the compare date to the end date (unless there is no end date!)
$compare_date = $end_date ?: $start_date;

// Creates an interval object,
// counts the # of days between the two dates,
// and sets invert to 1 if $today is after the $compare_date)
$interval = $compare_date->diff($today);
if ($interval->invert === 1 || is_singular()) {
?>

<article <?php post_class('event'); ?>>
    <div class="event-left">
        <div class="event-date">
            <h3 class="event-month">
                <?php echo $start_date->format('F');
                if (get_field('end_date')) {
                    echo ' - ' . $end_date->format('F');
                } ?>
            </h3>
            <h3 class="event-days">
                <?php echo $start_date->format('d');
                if (get_field('end_date')) {
                    echo ' - ' . $end_date->format('d');
                } ?>
            </h3>
            <?php if (get_field('hours')) : ?>
                <h4 class="event-hours">Hours</h4>
                <span class="event-time"><?php echo get_field('hours'); ?></span>
            <?php endif; ?>
        </div>
        <?php if (get_field('enable_rsvp')) { ?>
        <a href="#rsvp" data-event="<?php the_title(); ?>" class="btn btn--alt rsvp">RSVP</a>
        <?php } ?>
        <share-button id="share-button" class="btn">Share</share-button>
    </div>

    <div class="event-wrap">
        <?php echo Extras\ttg_featured_image($link = true); ?>
        <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?><?php if($interval->invert === 0) { echo '<br><span style="color: red; font-size: 18px;">(This Event has Ended)</span>'; } ?>
</a></h2>
        <hr class="event-hr">
        <div class="event-date-mobile">
            <?php // Date Config
            $start_date = DateTime::createFromFormat('Ymd', get_field('date'));
            $end_date   = DateTime::createFromFormat('Ymd', get_field('end_date'));
            ?>
            <h3 class="event-month">
                <?php echo $start_date->format('F d');
                if (get_field('end_date')) {
                    echo ' - ' . $end_date->format('F d');
                } ?>
            </h3>
            <span class="event-time"><?php echo get_field('hours'); ?></span>
            <hr class="event-hr">
        </div>
        <?php // get_template_part('templates/entry-meta'); ?>
        <div class="event-details">
            <?php if (is_archive()) {
                the_excerpt();
            } else {
                the_content();
            }
            ?>
        </div>
        <div class="event-ctas">
            <?php if (get_field('enable_rsvp')) { ?>
            <a href="#rsvp" data-event="<?php the_title(); ?>" class="btn rsvp">RSVP</a>
            <?php } ?>
            <?php if (have_rows('ctas')) : while(have_rows('ctas')): the_row(); ?>
                <a class="btn" href="<?php echo get_sub_field('cta_link'); ?>"><?php echo get_sub_field('cta_text'); ?></a>
            <?php endwhile; endif; ?>
        </div>
    </div>
</article>

<?php }
