<div id="financingModal" class="modal-wrapper">
    <div class="modal">
        <span class="modal-dismiss">+</span>
        <h3 id="modalTitle">Apply for Financing</h3>
        <div class="modal-inner js-active">
            <iframe src="https://d.comenity.net/iddjewelry/public/apply/Apply.xhtml" frameborder="0"></iframe>
        </div>
    </div>
</div>