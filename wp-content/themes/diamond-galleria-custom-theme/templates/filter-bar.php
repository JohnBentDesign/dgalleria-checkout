<?php
/**
 * Product Filtering and Sorting Navigation Bar
 */
?>
<aside class="filter-bar">
    <div class="filter-by">
        <h4 class="filter-title">Filter By:</h4>
        <?php dynamic_sidebar('product-filters'); ?>
    </div>
    <!-- Woocommerce code will be injected here via js later! -->
</aside>
