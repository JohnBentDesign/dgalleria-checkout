<div class="designer-products">
    <?php
    // Get all of the avaliable products for the current designer
    // IMPORTANT: The slug of the designer's custom post MUST
    // MATCH the slug of the designer's product category

    // Get the page slug
    $designer_slug = get_the_slug();

    // Get all of the avaliable Product Types
    // (There is a product category called "Product Types")
    $parent = 215;

    if ($_SERVER["SERVER_NAME"] == 'dgal.dev') {
        $parent = 55;
    }

    $product_categories = get_terms(array(
        'taxonomy' => 'product_cat',
        'hide_empty' => true,
        'orderby' => 'count',
        // The id of the Product Type
        // local
        'parent' => $parent,
        // live
        // 'parent' => 215,
        ));

    $count = count($product_categories);

    // If categories are avaliable, loop through them
    if ($count > 0) {
        foreach ($product_categories as $cat) {

            $args = array(
                'post_type' => 'product',
                'posts_per_page' => 5,
                'tax_query' => array(
                    'relation' => 'AND',
                    array(
                        'taxonomy' => 'product_cat',
                        'field' => 'slug',
                        'terms' => $designer_slug,
                        ),
                    array(
                        'taxonomy' => 'product_cat',
                        'field' => 'slug',
                        'terms' => $cat->slug,
                        ),
                    ),
                );

            $loop = new WP_Query($args);

            // If the category's product count is greater than 0
            // and it's not a parent category
            if ($cat->count > 0) {

                if ($loop->have_posts()) {

                    echo '<div class="border-row">';
                    echo '<div class="products-header">';
                    echo '<h2 class="products-title">' . $cat->name . '</h2>';
                    echo '<a class="btn btn--small view-more" href="/product-category/' . $designer_slug . '?pa_product_type='. $cat->slug .'">View All</a>';
                    echo '</div>';
                    echo '</div>';

                    while ($loop->have_posts()):
                        $loop->the_post();
                    wc_get_template_part('content', 'product');
                    endwhile;
                } else {
                }
                wp_reset_postdata();
            }
        }
    }
    ?>
</div>
