<?php while (have_posts()) :
    the_post(); ?>
    <?php get_template_part('templates/content', 'events'); ?>
<?php endwhile;

echo '<div class="past-events"><h2>Past Events</h2><hr class="event-hr">';

while (have_posts()) :
    the_post();
    // Date Config
    $today      = new DateTime('NOW');
    $today->modify('-1 day');
    $start_date = DateTime::createFromFormat('Ymd', get_field('date'));
    $end_date   = DateTime::createFromFormat('Ymd', get_field('end_date'));

    // Set the compare date to the end date (unless there is no end date!)
    $compare_date = $end_date ?: $start_date;

    // Creates an interval object,
    // counts the # of days between the two dates,
    // and sets invert to 1 if $today is after the $compare_date)
    $interval = $compare_date->diff($today);
    if ($interval->invert === 0) {
        echo '<a class="past-event" href="' . get_the_permalink() . '">' . get_the_title() . ' - ' . $start_date->format('F Y') . '</a>';
    }

endwhile;

echo '</div>';

the_posts_navigation();

