<?php
// Get fields
$desginersCount = wp_count_posts('designers')->publish;
$designersArgs = array(
    'post_type'      => 'designers',
    'post_status'    => 'publish',
    'posts_per_page' => -1,
    'order'          => 'ASC',
    'orderby'        => 'menu_order',
);

$designers = new WP_Query($designersArgs);

if ($designers->have_posts()) :
?>

<div class="sectional designers">
    <?php
    $i = 0;
    while ($designers->have_posts()) :

        $designers->the_post();
        $i++;

        if ($i % 2 !== 0) {
            echo '<div class="section">';
        }

        // Images
        $imageLogo = get_field('designer_logo') ? wp_get_attachment_image(get_field('designer_logo'), 'large', 0) : '<h3>' . get_the_title($designers->ID) . '</h3>';
        $imageFeatured = get_field('landing_page') ? wp_get_attachment_image(get_field('landing_page'), 'large', 0) : get_the_post_thumbnail($designers->ID);
    ?>
	    <div class="block block--half">
	        <a href="<?=get_permalink();?>">
	            <div class="section">
	                <div class="block block--half">
	                    <?=$imageLogo;?>
	                </div>
	                <div class="block block--half">
	                    <?=$imageFeatured;?>
	                </div>
	            </div>
	        </a>
	    </div>
	    <?php
        if (($i % 2 == 0) || (($i % 2 != 0) && ($i == $designersCount))) {
            echo '</div>';
        }
    endwhile;
?>
</div>

<?php
endif;
wp_reset_postdata();
