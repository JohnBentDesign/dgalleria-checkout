<?php

namespace Roots\Sage\Customizer;

use Roots\Sage\Assets;

/**
 * Register Customizer
 */
function customize_register($wp_customize) {

  // Config (Change these settings per-site)
  $ttg_social_networks = \ttgConfig::$social_networks;

  // PostMessage Support for Title
  $wp_customize->get_setting('blogname')->transport = 'postMessage';

  // Add Site Settings Panel
  $wp_customize->add_panel('ttg_general_settings', array(
      'priority'       => 2,
      'capability'     => 'edit_theme_options',
      'theme_supports' => '',
      'title'          => 'General Settings',
      'description'    => 'General and Company Settings',
  ));

  // Move Default Options
  $wp_customize->get_section('title_tagline')->panel = "ttg_general_settings";
  $wp_customize->get_section('static_front_page')->panel = "ttg_general_settings";

  // Logo Section
  $wp_customize->add_section('ttg_logo' , array(
    'title'      => __('Logo','sage'),
    'panel'      => __('ttg_general_settings')
  ));

  $wp_customize->add_setting('ttg_add_logo');
  $wp_customize->add_control(new \WP_Customize_Media_Control($wp_customize,'ttg_add_logo', array(
    'label'      => __('Site Logo', 'sage'),
    'section'    => 'title_tagline',
    'mimetype'   => 'image'
  )));

  // Social Media Section
  $wp_customize->add_section('ttg_social_media' , array(
    'title'      => __('Social Media Profiles','sage'),
    'panel'      => __('ttg_general_settings')
  ));

  // Social Media Profile Urls
  foreach ($ttg_social_networks as $social_network) {
    $wp_customize->add_setting('ttg_' . $social_network, array(
      'transport'         => 'postMessage'
    ));
    $wp_customize->add_control('ttg_' . $social_network, array(
      'label'       => __(ucfirst($social_network) . ' Link', 'sage'),
      'section'     => 'ttg_social_media',
      'type'        => 'text',
      'priority'    => 3,
    ));
  }

}
add_action('customize_register', __NAMESPACE__ . '\\customize_register');

/**
 * Customizer JS
 */
function customize_preview_js() {
  wp_enqueue_script('sage/customizer', Assets\asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
}
add_action('customize_preview_init', __NAMESPACE__ . '\\customize_preview_js');
