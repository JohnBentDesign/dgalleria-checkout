<?php
// Remove some WooCommerce Defaults, like breadcrumbs
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
// and their stylesheet
// add_filter('woocommerce_enqueue_styles', '__return_empty_array');

// Remove add to cart button on the navigation bar
//remove_action( 'woo_nav_after', 'wootique_cart_button', 10);
//remove_action( 'woo_nav_after', 'wootique_checkout_button', 20);

// Remove add to cart button from the product loop
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10, 2);

// Remove add to cart button from the product details page
//remove_action( 'woocommerce_before_add_to_cart_form', 'woocommerce_template_single_product_add_to_cart', 10, 2);

// disabled actions (add to cart, checkout and pay)
//remove_action( 'init', 'woocommerce_add_to_cart_action', 10);
//remove_action( 'init', 'woocommerce_checkout_action', 10 );
//remove_action( 'init', 'woocommerce_pay_action', 10 );

// Declare WooCommerce Support
add_action('after_setup_theme', 'woocommerce_support');
function woocommerce_support()
{
    add_theme_support('woocommerce');
}

// Helper function to get slugs from post IDs
function get_the_slug($id = null)
{
    if (empty($id)):
        global $post;

        if (empty($post)) {
            return ''; // No global $post var available.
        }

        $id = $post->ID;
    endif;

    $slug = basename(get_permalink($id));
    return $slug;
}

// Display 24 products per page.
add_filter('loop_shop_per_page', create_function('$cols', 'return 24;'), 24);

// Change number or products per row
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
    function loop_columns()
    {
        return 5; // 3 products per row
    }
}

// Ring Sizing functionality

// Generate input to display in add to cart form
function ring_custom_sizing() {
   global $post, $product;
   $categories = $product->get_categories();
   // check to see if categories string contains Rings
   if(strpos($categories, 'Rings') !== false || strpos($categories, 'Band') !== false ) {
?>
    <div class="size_select">
        <span>Size: </span><input class="input_size ring_size" name="ring_size" step=".5" min="4" max="7.5" title="size" size="4" type="number" value="4.5"/>
    </div>
<?php
    } else {
        return;
    }
}
add_action('woocommerce_before_add_to_cart_button', 'ring_custom_sizing');

// save ring size to wp post metadata

function save_ring_custom_data( $cart_item_data, $product_id ) {
    if( isset( $_REQUEST['ring_size'] ) ) {
        $cart_item_data['ring_size'] = $_REQUEST['ring_size'];
        $cart_item_data['unique_key'] = md5(microtime().rand());
    }
    return $cart_item_data;
}
add_action('woocommerce_add_cart_item_data', 'save_ring_custom_data', 10, 2 );

// pull saved ring size data and display on cart page

function render_ring_meta_on_cart_and_checkout( $cart_data, $cart_item = null ) {
    $custom_items = array();

    if( !empty( $cart_data ) ) {
        $custom_items = $cart_data;
    }
    if( isset( $cart_item['ring_size'] ) ) {
        $custom_items[] = array(
            'name' => 'Size',
            'value' => $cart_item['ring_size']
            );
    }
    return $custom_items;
}
add_filter('woocommerce_get_item_data', 'render_ring_meta_on_cart_and_checkout', 10, 2);

// update cart count in header 

/**
 * Ensure cart contents update when products are added to the cart via AJAX
 */
function my_header_add_to_cart_fragment( $fragments ) {
 
    ob_start();
    $count = WC()->cart->cart_contents_count;

    ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php if ( $count > 0 ) echo '(' . $count . ')'; ?></a><?php
 
    $fragments['a.cart-contents'] = ob_get_clean();
     
    return $fragments;
}
add_filter( 'woocommerce_add_to_cart_fragments', 'my_header_add_to_cart_fragment' );

// Custom Price messages 

add_filter( 'woocommerce_variable_free_price_html','remove_free_price_text' );

add_filter( 'woocommerce_free_price_html','remove_free_price_text' );

add_filter( 'woocommerce_variation_free_price_html','remove_free_price_text' );

function remove_free_price_text( $price ) {
  return 'Call For Price';
}

/**
* Gravity Forms Custom Activation Template
* http://gravitywiz.com/customizing-gravity-forms-user-registration-activation-page
*/
add_action('wp', 'custom_maybe_activate_user', 9);
function custom_maybe_activate_user() {

    $template_path = STYLESHEETPATH . '/gfur-activate-template/activate.php';
    $is_activate_page = isset( $_GET['page'] ) && $_GET['page'] == 'gf_activation';

    if( ! file_exists( $template_path ) || ! $is_activate_page  )
        return;

    require_once( $template_path );

    exit();
}

// Move location of payment options on checkout page

remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );

add_action( 'woocommerce_after_order_notes', 'woocommerce_checkout_payment', 20 );

?>