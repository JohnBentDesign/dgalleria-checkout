<?php

// Social Networks
class ttgConfig {
  /**
   * An array of social networks for use in share and add buttons
   * @var [type]
   */
  public static $social_networks = [
    'facebook',
    'twitter',
    'youtube',
    'google-plus',
    'pinterest',
    'instagram',
    'foursquare'
  ];
}
