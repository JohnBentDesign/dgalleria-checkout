<?php

namespace Roots\Sage\ACF;

// Register Options Page
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page();
}