<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
 * Displays an optional post thumbnail.
 *
 * Wraps the post thumbnail in an anchor element on index views, or a div
 * element when on single views.
 *
 * Create your own ttg_post_thumbnail() function to override in a child theme.
 *
 */
function ttg_featured_image($link = true) {
  if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
    return;
  }
  if ( is_singular() ) :
  ?>

  <div class="featured-image">
    <?php the_post_thumbnail(); ?>
  </div><!-- .post-thumbnail -->

  <?php else : ?>

    <?php if ($link) : ?>
        <a class="featured-image" href="<?php the_permalink(); ?>" aria-hidden="true">
            <?php the_post_thumbnail( 'full', array( 'alt' => the_title_attribute( 'echo=0' ) ) ); ?>
        </a>
    <?php else : ?>
        <?php the_post_thumbnail( 'full', array( 'alt' => the_title_attribute( 'echo=0' ) ) ); ?>
    <?php endif; ?>

  <?php endif; // End is_singular()
}

function svg($iconname, $echo = true) {
  $path = 'dist/svg/' . $iconname . '.svg';
  if($echo) {
    get_template_part($path);
  } else {
    ob_start();
    get_template_part($path);
    $var = ob_get_contents();
    ob_end_clean();
    return $var;
  }
}

/**
 * Grabs a predertimined list of social networks,
 * and displays an associated SVG icon,
 * if the user has provided a social link
 * in the WordPress customizer
 *
 * @param  [type] $text [description]
 * @return [type]       [description]
 */
function social_icons($text = null) {

  $social_networks = \ttgConfig::$social_networks; ?>

  <div class="social-icons">
    <?php if ($text) : ?>
      <span class="social-text"><?php echo $text; ?></span>
    <?php endif; ?>

    <?php foreach ($social_networks as $network) :

      $network_url = get_theme_mod('ttg_' . $network);

      if ($network_url) :
    ?>
      <a href="<?php echo $network_url; ?>"><?php svg($network); ?></a>
    <?php endif;
    endforeach; ?>
  </div> <?php
}

// Add Image Size
add_image_size('blog-featured', 860, 360, true);
