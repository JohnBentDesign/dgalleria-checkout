<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup()
{
    // Enable features from Soil when plugin is activated
    // https://roots.io/plugins/soil/
    add_theme_support('soil-clean-up');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-relative-urls');
    add_theme_support('soil-js-to-footer');

    // Make theme available for translation
    // Community translations can be found at https://github.com/roots/sage-translations
    load_theme_textdomain('sage', get_template_directory() . '/lang');

    // Enable plugins to manage the document title
    // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
    add_theme_support('title-tag');

    // Register wp_nav_menu() menus
    // http://codex.wordpress.org/Function_Reference/register_nav_menus
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage'),
        'footer_navigation_1' => __('Footer Collections', 'sage'),
        'footer_navigation_2' => __('Footer Navigation', 'sage'),
    ]);

    // Enable post thumbnails
    // http://codex.wordpress.org/Post_Thumbnails
    // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
    // http://codex.wordpress.org/Function_Reference/add_image_size
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(1800, 0, true);

    // Enable post formats
    // http://codex.wordpress.org/Post_Formats
    add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

    // Enable HTML5 markup support
    // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    // Use main stylesheet for visual editor
    // To add custom styles edit /assets/styles/layouts/_tinymce.scss
    add_editor_style(Assets\asset_path('styles/main.css'));

}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Register sidebars
 */
function widgets_init()
{
    register_sidebar([
        'name' => __('Primary', 'sage'),
        'id' => 'sidebar-primary',
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ]);

    register_sidebar([
        'name' => __('Footer', 'sage'),
        'id' => 'sidebar-footer',
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ]);

    register_sidebar([
        'name' => __('Product Filters', 'sage'),
        'id' => 'product-filters',
        'before_widget' => '<div class="filter-widget widget %1$s %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="filter-title">',
        'after_title' => '</h3>',
    ]);
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar()
{
    static $display;

    isset($display) || $display = !in_array(true, [
        // The sidebar will NOT be displayed if ANY of the following return true.
        // @link https://codex.wordpress.org/Conditional_Tags
        is_404(),
        is_front_page(),
        is_page_template('template-custom.php'),
        is_page(),
        is_singular('designers'),
        is_singular('events'),
        is_singular('product'),
        // is_single(),
        is_archive(),
    ]);

    return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets()
{
    wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, null);

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    wp_enqueue_script('sage/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);

}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);

/**
 * Admin-only Assets
 */
function admin_assets()
{
    wp_enqueue_style('sage/admin-css', Assets\asset_path('styles/admin.css'), false, null);
    wp_enqueue_script('sage/acf-seo-js', Assets\asset_path('scripts/acf-yoast-seo-validation.js'), ['jquery'], null, true);
}
add_action('admin_enqueue_scripts', __NAMESPACE__ . '\\admin_assets', 100);

add_filter("gform_init_scripts_footer", __NAMESPACE__ . "\\init_scripts");
function init_scripts()
{
    return true;
}

function presort_events($query)
{
    // do not modify queries in the admin
    if (is_admin()) {
        return $query;
    }

    // only modify queries for 'event' post type
    if (isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == 'events') {
        $query->set('orderby', 'meta_value_num');
        $query->set('meta_key', 'date');
        $query->set('order', 'ASC');
    }

    // return
    return $query;
}
add_action('pre_get_posts', __NAMESPACE__ . '\\presort_events');

function my_body_class($classes)
{
    if (is_singular('events') || is_archive('events')) {
        $classes[] = 'events-rsvp';
    }
    return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\my_body_class');

function get_social_networks()
{
    return array(
        'facebook' => 'http://www.facebook.com/sharer/sharer.php?u=' . urlencode(get_permalink()) . '&title=' . urlencode(get_the_title()) . '',
        'google-plus' => 'https://plus.google.com/share?url=' . urlencode(get_permalink()) . '',
        'linkedin2' => 'http://www.linkedin.com/shareArticle?mini=true&url=' . urlencode(get_permalink()) . '&title=' . urlencode(get_the_title()) . '&source=[SOURCE/DOMAIN]',
        'letter' => 'mailto:?subject=' . urlencode(get_the_title()) . '&body=' . urlencode(get_permalink()) . '" target="_top"',
        'pinterest' => 'http://pinterest.com/pin/create/bookmarklet/&url=' . urlencode(get_permalink()) . '&is_video=false&description=' . urlencode(get_the_title()) . '',
        'twitter' => 'http://twitter.com/intent/tweet?status=' . urlencode(get_the_title()) . ' + ' . urlencode(get_permalink()) . '',
    );
}

// Posts per event query
function events_per_page($query)
{
    if ($query->query_vars['post_type'] === 'events') {
        $query->query_vars['posts_per_page'] = 100;
    }
    return $query;
}

if (!is_admin()) {
    add_filter('pre_get_posts', __NAMESPACE__ . '\\events_per_page');
}

// Custom Gravityform Url Paramaeter
add_filter('gform_field_value_page_url', function ($value) {
    return get_permalink();
});


