<?php
/*==================================================================
BUILD A SECTION

Like build a bear, but not as cute ʕっ•ᴥ•ʔっ
=================================================================*/

class section_build
{

    //----------------------------------------------------------------
    // CONSTRUCT THE SECTION
    //----------------------------------------------------------------
  public $section = null;
  public function __construct($section)
  {
    $this->section = $section;
  }

    //----------------------------------------------------------------
    // CONTENT CONSTRUCTION
    //----------------------------------------------------------------
    // CONTENT: Title
    //--------------------------------
  private function content_title()
  {
    $sectionWidth = $this->section['section_width'];
    $sectionSmall = (($sectionWidth == 'third') || ($sectionWidth == 'half')) ? true : false;
    $contentTitle = $this->section['content_title'];
    $titleOverlay = $this->section['title_overlay_display'];
    $titleColor = $this->section['title_overlay_color'];

        // Display only if there's a title
    if ($contentTitle) {

            // Wrap in Overlay if Allowed
      if ($sectionSmall && $titleOverlay) {
        echo '<div class="overlay--' . $titleColor . ' title-overlay">';
      }

            // Custom Title
      echo '<h2>' . $contentTitle . '</h2>';

            // Wrap in Overlay if Allowed
      if ($sectionSmall && $titleOverlay) {
        echo '</div>';
      }

    }

        // Nanimo...
    else {
      return false;
    }
  }

        // CONTENT: Content
        //--------------------------------
  private function content_content()
  {
    $contentContent = $this->section['content_content'];

            // Custom Content
    if ($contentContent) {
      echo $contentContent;
    }

            // Nanimo...
    else {return false;}
  }

        // CONTENT: Words
        //--------------------------------
  private function content_words()
  {
            // This combines the title, content, and cta button into one element
    $wordsWidth = $this->style_inner_width();

    $this->content_title();
    $this->content_content();
    $this->cta_btn();
  }

        //----------------------------------------------------------------
        // IMAGE CONSTRUCTION
        //----------------------------------------------------------------
        // IMAGE: Image
        //--------------------------------
  private function image_image()
  {
    $sectionWidth = $this->section['section_width'];
    $imageDisplay = $this->section['image_display'];
    $imageImage = $this->section['image_image'];
    $imageSize = $this->style_inner_width();
    $equalize = (($sectionWidth == 'two-third') || ($sectionWidth == 'full')) ? ' data-equalizer-watch' : '';
    $titleOverlay = $this->section['title_overlay_display'];

    if ($imageDisplay) {
                // Custom Image
      $hasImage = wp_get_attachment_image($imageImage, 'post-thumbnail', 0, array('title' => get_the_title($imageImage)));

      if ($imageImage) {
        if ($equalize) {
          echo '<div class="block image-container ' . $imageSize['image'] . '"' . $equalize . '>';
        } else {
          echo '<div class="image-container ' . $imageSize['image'] . '">';
        }

                    // Display image
        echo $hasImage;

                    // Display title if available
        if (!$equalize && $titleOverlay) {
          echo '<div class="title-overlay">';
          $this->content_title();
          echo '</div>';
        }

        echo '</div>';
      }
    }
            // Nanimo...
    else {return false;}
  }

        //----------------------------------------------------------------
        // CTA CONSTRUCTION
        //----------------------------------------------------------------
        // CTA: Button
        //--------------------------------
  private function cta_btn()
  {
    $ctaDisplay = $this->section['cta_display'];
    $ctaType = $this->section['cta_type_display'];

            // Custom CTA
    if ($ctaDisplay) {
                // Get all the CTA settings
      $ctaTitle = $this->section['cta_text'];
      $ctaLinkType = $this->section['cta_type'];
      $ctaLink = ($ctaLinkType == 'internal') ? $this->section['cta_internal'] : $this->section['cta_external'];
      $ctaBlank = ($this->section['cta_blank']) ? ' target="_blank"' : '';
      $ctaDisplayType = ($this->section['cta_type_display'] == 'button') ? 'btn' : 'txt';

                // Output the link
      echo '<a href="' . $ctaLink . '" class="cta ' . $ctaDisplayType . '"' . $ctaBlank . '>' . $ctaTitle . '</a>';
    }
            // Nanimo...
    else {return false;}
  }

        //----------------------------------------------------------------
        // STYLING CONSTRUCTION
        //----------------------------------------------------------------
        // STYLING: Background Color
        //--------------------------------
  private function style_bgcolor()
  {
    $bgChange = $this->section['bg_change'];
    $bgChangeType = $this->section['bg_change_type'];

            // Custom Color
    if ($bgChange && ($bgChangeType == 'color')) {
      return ' ' . $this->section['bg_color'];
    }
            // Defaults to Primary
    else {
      return ' primary';
    }
  }

        // STYLING: Background Image
        //--------------------------------
  private function style_bgimage()
  {
    $bgChange = $this->section['bg_change'];
    $bgChangeType = $this->section['bg_change_type'];
    $bgImage = $this->section['bg_image'];
    $bgFixed = ($this->section['bg_parallax']) ? ' background-attachment: fixed;' : '';

            // Background image
    if ($bgChange && ($bgChangeType == 'image') && $bgImage) {
      return ' style="background-image: url(' . wp_get_attachment_image_src($bgImage, 'post-thumbnail')[0] . ');' . $bgFixed . '"';
    }
            // Nanimo...
    else {return false;}
  }

        // STYLING: Background Overlay
        //--------------------------------
  private function style_overlay()
  {
    $bgChange = $this->section['bg_change'];
    $bgChangeType = $this->section['bg_change_type'];
    $bgOverlay = $this->section['bg_overlay'];

            // Get background image
    if ($bgChange && ($bgChangeType == 'image')) {
      return $bgOverlay;
    }
            // Nanimo...
    else {return false;}
  }

        // STYLING: Section Margin
        //--------------------------------
  private function style_margin()
  {
    $marginLeft = $this->section['content_margin_left'] ? 'padding-left: ' . $this->section['content_margin_left'] . '%;' : '';
    $marginRight = $this->section['content_margin_right'] ? 'padding-right: ' . $this->section['content_margin_right'] . '%;' : '';
    $marginTop = $this->section['content_margin_top'] ? 'padding-top: ' . $this->section['content_margin_top'] . 'px;' : '';
    $marginBtm = $this->section['content_margin_bottom'] ? 'padding-bottom: ' . $this->section['content_margin_bottom'] . 'px;' : '';

            // Get margins
    if ($marginLeft || $marginRight || $marginTop || $marginBtm) {
      return 'style="' . $marginLeft . $marginRight . $marginTop . $marginBtm . '"';
    }
            // Nanimo...
    else {return false;}
  }

        // STYLING: Section Width
        //--------------------------------
  private function style_section_width()
  {
    $sectionWidth = $this->section['section_width'];

    return 'block--' . $sectionWidth;
  }

        // STYLING: Inner Grid Sizes
        //--------------------------------
  private function style_inner_width()
  {
            // This function helps us build our grid sizes. We have a few things to consider: content, image, lists, and CTA boxes
            // All of these things have the chance to change sides and sizes, and need to be strong armed at certain sizes to make sure the sizes aren't crazy
            // So to keep things neat, the front end code is done as follows:
            // [ row ]
            //      [ column: image ]
            //      [ column: all content ]
            //          [ row ]
            //              [ column: text content ]
            //              [ column: cta (box) ]
            //              [ column: list content ]

            // Section Width
    $sectionWidth = $this->section['section_width'];
    $sectionSmall = (($sectionWidth == 'third') || ($sectionWidth == 'half')) ? true : false;

            // Image Width
    $imageDisplay = $this->section['image_display'];
    $imageSize = $this->section['image_size'];
    $imageSide = $this->section['image_side'];

            // Inner grid variables
    $ctaDisplay = $this->section['cta_display'];
    $ctaType = $this->section['cta_display_type'];

            // Class Array
    $classes = array();
    $classes['image'] .= ' component-image';
    $classes['content'] .= ' component-content';
    $classes['words'] .= ' component-words';
    $classes['cta'] .= ' component-cta';

            // Translate our sizes into grids
    if ($imageDisplay && !$sectionSmall) {
      switch ($imageSize) {
        case 'third':
        $classes['image'] .= ' block--third block';
        $classes['content'] .= ' block--two-third block';
                        // We need to change the positioning if the image is on the right
        if ($imageSide == 'right') {
          $classes['image'] .= ' block--force-right';
          $classes['content'] .= ' block--force-left';
        }
        break;
        case 'half':
        $classes['image'] .= ' block--half block';
        $classes['content'] .= ' block--half block';
                        // We need to change the positioning if the image is on the right
        if ($imageSide == 'right') {
          $classes['image'] .= ' block--force-right';
          $classes['content'] .= ' block--force-left';
        }
        break;
        case 'two-third':
        $classes['image'] .= ' block--two-third block';
        $classes['content'] .= ' block--third block';
                        // We need to change the positioning if the image is on the right
        if ($imageSide == 'right') {
          $classes['image'] .= ' block--force-right';
          $classes['content'] .= ' block--force-left';
        }
        break;
        case 'full':
        $classes['image'] .= ' block--full block';
        $classes['content'] .= ' block--full block';
                        // We need to change the positioning if the image is on the right
        if ($imageSide == 'right') {
          $classes['image'] .= ' block--force-right';
          $classes['content'] .= ' block--force-left';
        }
        break;
      }
    } else {
      $classes['content'] .= ' block block--full';
    }

    return $classes;
  }

        //----------------------------------------------------------------
        // MARK-UP CONSTRUCTION
        //----------------------------------------------------------------
        // SECTION: Output
        //--------------------------------
  public function section_construct($index)
  {
    global $post;

            // Section slug
    $sectionSlug = sanitize_title($this->section['section_slug']);

            // Section width variables
    $sectionWidth = $this->section['section_width'];
    $sectionSmall = (($sectionWidth == 'third') || ($sectionWidth == 'half')) ? true : false;
    $sectionSize = (($sectionWidth == 'third') || ($sectionWidth == 'half')) ? ' block--smaller' : 'block--larger';

            // Section styling variables
    $sectionPadding = $this->section['section_padding'] ? ' gutterless' : '';
    $sectionCenter = $this->section['content_center'] ? ' text-center' : '';
    $sectionTextColor = ($this->section['content_color'] != 'default') ? ' text--' . $this->section['content_color'] : '';
    $sectionBlockClass = $this->style_section_width() . $sectionCenter . $sectionPadding . $sectionTextColor . $this->style_bgcolor();

            // Overlay Container
    $overlayOpen = ($this->style_overlay()) ? '<div class="overlay overlay--' . $this->style_overlay() . '">' : '';
    $overlayClose = ($this->style_overlay()) ? '</div>' : '';

            // Background Container
    $bgOpen = $this->style_bgimage() ? '<div class="has-background" ' . $this->style_bgimage() . '>' : '';
    $bgClose = $this->style_bgimage() ? '</div>' : '';
    ?>

    <div class="block <?=$sectionBlockClass;?>" id="<?=$sectionSlug;?>" data-equalizer-watch>

      <?php
    // SMALL SECTION
            //--------------------------------
      if ($sectionSmall):
        $titleOverlay = $this->section['title_overlay_display'];
      ?>
      <?php $this->image_image();?>

      <?=$bgOpen;?>
      <?=$overlayOpen;?>
      <div class="inner clearfix"<?=$this->style_margin();?>>
       <?php
       if (!$titleOverlay) {
        $this->content_title();
      }
      $this->content_content();
      $this->cta_btn();
      ?>
    </div>
    <?=$overlayClose;?>
    <?=$bgClose;?>

    <?php
        // LARGE SECTION
                //--------------------------------
    else:
      $gridClass = $this->style_inner_width();
    $hasImage = $this->section['image_image'];
    ?>
    <div class="section" data-equalizer>
     <?php $this->image_image();?>

     <?=$bgOpen;?>
     <?=$overlayOpen;?>
     <div class="<?=$gridClass['content'];?>" data-equalizer-watch <?=$this->style_margin();?>>
      <?php
      $this->content_words();
      ?>
    </div>
    <?=$overlayClose;?>
    <?=$bgClose;?>
  </div>

<?php endif;?>
</div>
<?php
}

}