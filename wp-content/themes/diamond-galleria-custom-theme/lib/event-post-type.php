<?php

namespace Roots\Sage\eventPostType;

/**
 * Register event Post Type
 */
function register_event_post_type() {

    $labels = array(
        'name'                  => _x( 'Events', 'Post Type General Name', 'sage' ),
        'singular_name'         => _x( 'Event', 'Post Type Singular Name', 'sage' ),
        'menu_name'             => __( 'Events', 'sage' ),
        'name_admin_bar'        => __( 'Events', 'sage' ),
        'parent_item_colon'     => __( 'Parent Item:', 'sage' ),
        'all_items'             => __( 'All events', 'sage' ),
        'add_new_item'          => __( 'Add event', 'sage' ),
        'add_new'               => __( 'Add event', 'sage' ),
        'new_item'              => __( 'New event', 'sage' ),
        'Edit_item'             => __( 'Edit event', 'sage' ),
        'update_item'           => __( 'Update event', 'sage' ),
        'view_item'             => __( 'View events', 'sage' ),
        'search_items'          => __( 'Search events', 'sage' ),
        'not_found'             => __( 'Not found', 'sage' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'sage' ),
        'items_list'            => __( 'Items list', 'sage' ),
        'items_list_navigation' => __( 'Items list navigation', 'sage' ),
        'filter_items_list'     => __( 'Filter items list', 'sage' ),
    );
    $args = array(
        'label'                 => __( 'Event', 'sage' ),
        'description'           => __( 'Post type for Ebowert vents', 'sage' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'page-attributes' ),
        'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-tickets-alt',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => 'events',
        'Exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'events', $args );

}
add_action( 'init',  __NAMESPACE__ . '\\register_event_post_type', 0 );