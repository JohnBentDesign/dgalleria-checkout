<?php

namespace Roots\Sage\DesignerPostType;

/**
 * Register Designer Post Type
 */
function register_designer_post_type() {

    $labels = array(
        'name'                  => _x( 'Designers', 'Post Type General Name', 'sage' ),
        'singular_name'         => _x( 'Designer', 'Post Type Singular Name', 'sage' ),
        'menu_name'             => __( 'Designers', 'sage' ),
        'name_admin_bar'        => __( 'Designers', 'sage' ),
        'parent_item_colon'     => __( 'Parent Item:', 'sage' ),
        'all_items'             => __( 'All Designers', 'sage' ),
        'add_new_item'          => __( 'Add Designer', 'sage' ),
        'add_new'               => __( 'Add Designer', 'sage' ),
        'new_item'              => __( 'New Designer', 'sage' ),
        'edit_item'             => __( 'Edit Designer', 'sage' ),
        'update_item'           => __( 'Update Designer', 'sage' ),
        'view_item'             => __( 'View Designers', 'sage' ),
        'search_items'          => __( 'Search Designers', 'sage' ),
        'not_found'             => __( 'Not found', 'sage' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'sage' ),
        'items_list'            => __( 'Items list', 'sage' ),
        'items_list_navigation' => __( 'Items list navigation', 'sage' ),
        'filter_items_list'     => __( 'Filter items list', 'sage' ),
    );
    $args = array(
        'label'                 => __( 'Designer', 'sage' ),
        'description'           => __( 'Post type for Designers', 'sage' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'page-attributes' ),
        'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        // 'menu_icon'             => '',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'Designers', $args );

}
add_action( 'init',  __NAMESPACE__ . '\\register_designer_post_type', 0 );